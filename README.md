# HTML TEMPLATE (DỊCH VỤ SỐ)
### Hướng dẫn sử dụng:
##### Bước 1: Tải hoặc clone về
- Clone git với SSH key: `git@gitlab.com:pein1625/dichvuso4.git`
- Clone git với HTTPS: `https://gitlab.com/pein1625/dichvuso4.git`
- Link tải: [https://gitlab.com/pein1625/dichvuso4/-/archive/master/dichvuso4-master.zip](https://gitlab.com/pein1625/dichvuso4/-/archive/master/dichvuso4-master.zip)
##### Bước 2: Cài đặt môi trường
- Mở terminal rồi trỏ đến thư mục của dự án
- Gõ lệnh `npm install`
##### Bước 3: Chạy chương trình
- Gõ lệnh `gulp dev` để bắt đầu code
- Gõ lệnh `gulp build` để build code
