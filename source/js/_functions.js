

$(function() {
  // checkbox expand
  $(".js-checkbox-expand").on("change", function() {
    const isChecked = $(this).prop("checked");
    const target = $(this).data("target");
    const isInvert = $(this).data("invert");

    if (isInvert ? !isChecked : isChecked) {
      $(target).slideDown();
    } else {
      $(target).slideUp();
    }
  });

  // menu toggle
  $(".js-menu-toggle").on("click", function() {
    const $toggle = $(this);
    const $sub = $toggle.siblings(".js-menu-sub");

    $toggle.toggleClass("active");
    $sub.slideToggle();
    $toggle
      .parent()
      .siblings()
      .children(".js-menu-sub")
      .slideUp();
  });
});

// allow number input
function numberInput(className) {
  var input = $(className);
  input.keydown(function(e) {
    if (
      $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
      (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
      (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
      (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
      (e.keyCode >= 35 && e.keyCode <= 39)
    ) {
      return;
    }
    if (
      (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) &&
      (e.keyCode < 96 || e.keyCode > 105)
    ) {
      e.preventDefault();
    }
  });
}

// floating
function floating() {
  $('.floating').each(function() {
    var $floating = $(this),
      width = $floating.width(),
      offsetLeft = $floating.offset().left,
      offsetTop = $floating.offset().top;

    $floating
      .data('offsetLeft', offsetLeft)
      .data('offsetTop', offsetTop)
      .css({
        width: width
      });
  });

  if ($(window).width() < 992) {
    return;
  }

  $(window).on('scroll', function() {
    $('.floating').each(function() {
      var $floating = $(this),
        offsetTop = $floating.data('offsetTop'),
        offsetLeft = $floating.data('offsetLeft'),
        height = $floating.outerHeight(),
        outerHeight = $floating.outerHeight(true),
        $container = $floating.closest('.floating-container'),
        dataTop = $floating.data('top'),
        top = dataTop !== undefined ? parseInt(dataTop) : 70,
        containerHeight = $container.outerHeight(),
        containerOffsetTop = $container.offset().top,
        scrollTop = $(window).scrollTop(),

      if (outerHeight + offsetTop == containerHeight + containerOffsetTop) {
        return;
      } else if (scrollTop + top <= offsetTop) {
        $(this).css({
          position: 'static'
        });
      } else if (
        scrollTop + height + top >
        containerHeight + containerOffsetTop
      ) {
        $(this).css({
          position: 'absolute',
          zIndex: 2,
          top: 'auto',
          bottom: 0,
          left: '15px'
        });
      } else {
        $(this).css({
          position: 'fixed',
          zIndex: 2,
          top: top,
          left: offsetLeft,
          bottom: 'auto'
        });
      }
    });
  });
}

// file input
document.addEventListener('DOMContentLoaded', function() {
  $('.chosen-select').chosen({
    width: '100%',
    allow_single_deselect: true
  });

  Array.from(document.querySelectorAll('.file-input__input'), function(input) {
    input.addEventListener('change', function() {
      const name = this.value.split(/\\|\//).pop();
      const truncated = name.length > 40 ? name.substr(name.length - 40) : name;
      this.nextSibling.innerHTML = truncated;
    });
  });
});

// count To
(function($) {
  $.fn.countTo = function(options) {
    // merge the default plugin settings with the custom options
    options = $.extend({}, $.fn.countTo.defaults, options || {});

    // how many times to update the value, and how much to increment the value on each update
    var loops = Math.ceil(options.speed / options.refreshInterval),
      increment = (options.to - options.from) / loops;

    return $(this).each(function() {
      var _this = this,
        loopCount = 0,
        value = options.from,
        interval = setInterval(updateTimer, options.refreshInterval);

      function updateTimer() {
        value += increment;
        loopCount++;
        $(_this).html(value.toFixed(options.decimals));

        if (typeof options.onUpdate == 'function') {
          options.onUpdate.call(_this, value);
        }

        if (loopCount >= loops) {
          clearInterval(interval);
          value = options.to;

          if (typeof options.onComplete == 'function') {
            options.onComplete.call(_this, value);
          }
        }
      }
    });
  };

  $.fn.countTo.defaults = {
    from: 0, // the number the element should start at
    to: 100, // the number the element should end at
    speed: 1000, // how long it should take to count between the target numbers
    refreshInterval: 100, // how often the element should be updated
    decimals: 0, // the number of decimal places to show
    onUpdate: null, // callback method for every time the element is updated,
    onComplete: null // callback method for when the element finishes updating
  };
})(jQuery);

jQuery(function($) {
  $('.js-count').each(function() {
    var count = parseInt($(this).data('count'));
    $(this).countTo({
      from: 0,
      to: count,
      speed: 3000,
      refreshInterval: 5
    });
  });
});

/*!
 * jQuery plugin
 * What does it do
 */
// (function($) {
//   $.fn.floating = function(opts) {
//     // default configuration
//     var config = $.extend(
//       {},
//       {
//         item: '.js-floating-item',
//         marginTop: 20,
//         containerPadding: 15
//       },
//       opts
//     );

//     // main function
//     function floating(e) {
//       var $window = $(window);

//       if ($window.width() < 992) {
//         return;
//       }

//       $window.on('scroll', function() {
//         e.each(function() {
//           var $container = $(this),
//             containerHeight = $container.outerHeight(),
//             containerOffsetTop = $container.offset().top,
//             $item = $container.find(config.item),
//             itemHeight = $item.outerHeight(),
//             itemOuterHeight = $item.outerHeight(true),
//             itemOffsetTop = $item.offset().top,
//             scrollTop = $window.scrollTop(),
//             margin = config.marginTop,
//             padding = config.containerPadding;

//           if (
//             itemOuterHeight + itemOffsetTop ==
//             containerHeight + containerOffsetTop
//           ) {
//             return;
//           } else if (scrollTop + margin <= itemOffsetTop) {
//             $item.css('position', 'static');
//           } else if (
//             scrollTop + itemHeight + margin >
//             containerHeight + containerOffsetTop
//           ) {
//             $item.css({
//               width: $container.width(),
//               position: 'absolute',
//               zIndex: 2,
//               top: 'auto',
//               bottom: 0,
//               left: padding
//             });
//           } else {
//             $item.css({
//               width: $container.width(),
//               position: 'fixed',
//               zIndex: 2,
//               top: margin,
//               left: $container.offset().left,
//               bottom: 'auto'
//             });
//           }
//         });
//       });
//     }

//     // initialize every element
//     this.each(function() {
//       floating($(this));
//     });

//     return this;
//   };

//   // start
//   $(function() {
//     $('.js-floating').floating();
//   });
// })(jQuery);

// smooth scroll
$(function() {
  $('.js-smooth-scroll').on('click', function(e) {
    e.preventDefault();

    var target = $(this).data('target') || $(this).attr('href'),
      offset = parseInt($(this).data('offset')) || 0,
      duration = parseInt($(this).data('duration')) || 800;

    $('html, body').animate(
      {
        scrollTop: $(target).offset().top - offset
      },
      duration
    );
  });
});

// vertical preview sync slider
$(function() {
  if (!$('.preview-slider, .thumb-slider').length) {
    return;
  }

  if (!window.addSwiper) {
    console.warn('"addSwiper" funtion is required!');
    return;
  }

  var thumbSlider = addSwiper('.thumb-slider', {
    direction: 'vertical',
    slidesPerView: 'auto',
    freeMode: true,
    watchSlidesProgress: true,
    watchSlidesVisibility: true,
    spaceBetween: 16,
    breakpoints: {
      576: {
        spaceBetween: 10
      }
    }
  })[0];

  addSwiper('.preview-slider', {
    effect: 'fade',
    allowTouchMove: false,
    thumbs: {
      swiper: thumbSlider
    }
  });
});

// horizontal preview sync slider
$(function() {
  if (!$('.preview-slider, .thumb-slider').length) {
    return;
  }

  if (!window.addSwiper) {
    console.warn('"addSwiper" funtion is required!');
    return;
  }

  var thumbSlider = addSwiper('.thumb-slider', {
    slidesPerView: 5,
    freeMode: true,
    spaceBetween: 16,
    watchSlidesProgress: true,
    watchSlidesVisibility: true,
    breakpoints: {
      576: {
        slidesPerView: 4,
        spaceBetween: 8
      }
    }
  })[0];

  addSwiper('.preview-slider', {
    effect: 'fade',
    allowTouchMove: false,
    thumbs: {
      swiper: thumbSlider
    }
  });
});
