// quantity input
$(function() {
  var $input = $('.quantity__input');
  var $btn = $('.quantity__btn');

  // only number
  $input.on('keydown', function(e) {
    if (
      $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
      (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
      (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
      (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
      (e.keyCode >= 35 && e.keyCode <= 39)
    ) {
      return;
    }
    if (
      (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) &&
      (e.keyCode < 96 || e.keyCode > 105)
    ) {
      e.preventDefault();
    }
  });

  // click to plus number
  $btn.on('click', function() {
    var $siblingInput = $(this).siblings('.quantity__input');
    var plus = $(this).data('plus');
    var value = $siblingInput.val();
    var newValue = parseInt(value) + plus;

    if (newValue > 0) {
      $siblingInput.val(newValue);
      $siblingInput.trigger('change');
    }
  });

  // prevent paste value
  $input.on('paste', function(e) {
    var paste = e.originalEvent.clipboardData.getData('text');
    var pasteNum = parseInt(paste);

    if (pasteNum > 0) {
      return;
    } else {
      e.preventDefault();
    }
  });

  $input.on('change', function() {
    var val = $(this).val();

    if (val && parseInt(val) > 0) {
      return;
    }

    $(this).val(1);
  });
});
