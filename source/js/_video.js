// open modal video
$(function() {
  $('.js-video-modal').on('click', function(e) {
    e.preventDefault();

    var youtubeId = $(this).data('youtubeId'),
      modal = $(this).data('modal') || '.md-video';

    $(modal)
      .find('iframe')
      .attr('src', `https://www.youtube.com/embed/${youtubeId}?autoplay=1`);
    $(modal).modal('show');
  });

  $('.md-video').on('hide.bs.modal', function() {
    $(this)
      .find('iframe')
      .attr('src', '');
  });
});

// open video switch
$(function() {
  $('.js-video-switch').on('click', function(e) {
    e.preventDefault();

    var target = $(this).data('target') || '.js-video-switch-target',
      youtubeId = $(this).data('youtubeId');

    $(target)
      .find('iframe')
      .attr('src', `https://www.youtube.com/embed/${youtubeId}?autoplay=1`);
  });
});
