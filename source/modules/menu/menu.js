// menu toggle
$(function() {
  $(".menu-toggle").on("click", function() {
    var $toggle = $(this);

    $toggle
      .toggleClass("active")
      .siblings(".menu-sub")
      .slideToggle();

    $toggle
      .siblings(".menu-mega")
      .children(".menu-sub")
      .slideToggle();

    $toggle
      .parent()
      .siblings(".menu-item-group")
      .children(".menu-sub")
      .slideUp();

    $toggle
      .parent()
      .siblings(".menu-item-group")
      .children(".menu-mega")
      .children(".menu-sub")
      .slideUp();

    $toggle
      .parent()
      .siblings(".menu-item-group")
      .children(".menu-toggle")
      .removeClass("active");
  });
});
